//==========================================
//puerto
//==========================================

process.env.PORT = process.env.PORT || 3000;

//==========================================
//entorno
//==========================================

process.env.NODE_ENV = process.env.NODE_ENV || 'dev';


//==========================================
//entorno
//==========================================

process.env.CLIENT_ID = process.env.CLIENT_ID || '147090511034-oecvntev4b80tkjja35el2vofnj4pqoi.apps.googleusercontent.com';

//==========================================
// Vencimiento del token
//==========================================
//60 segudos * 60 minutis * 24 horas * 30 dias
process.env.CADUCIDAD_TOKEN = "48h"


//==========================================
// SEED de autenticacion
//==========================================

process.env.SEED = process.env.SEED || 'este-es-seed-desarrollo';

//==========================================
// Base de datos
//==========================================

let urLDB;
if (process.env.NODE_ENV === 'dev') {
    urLDB = 'mongodb://localhost:27017/cafe';
} else {
    urLDB = process.env.MONGO_URL;
}

process.env.URLDB = urLDB;