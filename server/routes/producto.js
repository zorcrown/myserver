const express = require('express');
const { verificarToken } = require('../middlewares/autenticacion');
let app = express();
let Producto = require('..//models/producto');

//====================================
//           Obtener productos
//====================================
app.get('/productos', verificarToken, (req, res) => {
    // populate: usuario categoria
    // paginado

    let desde = req.params.desde || 0;
    let hasta = req.params.hasta || 5;
    desde = Number(desde);
    hasta = Number(hasta);
    Producto.find({ disponible: true })
        .sort('descripcion')
        .populate('categoria', 'descripcion nombre ')
        .populate('usuario', 'nombre email')
        .skip(desde)
        .limit(hasta)
        .exec((err, productos) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            Producto.count({ estado: true }, (err, conteo) => {
                res.json({
                    ok: true,
                    productos,
                    cuantos: conteo
                });
            })
        });


});

//====================================
//       Obtener producto por id
//====================================
app.get('/productos/:id', verificarToken, (req, res) => {
    // populate: usuario categoria
    // paginado

    let id = req.params.id;

    Producto.findById(id)
        .populate('usuario', 'usuario email')
        .populate('categoria', 'nombre')
        .exec((err, productoDB) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err
                });
            }

            if (!productoDB) {
                return res.status(400).json({
                    ok: false,
                    err: {
                        message: 'El id no es valido'
                    }
                });
            } else {
                res.json({
                    ok: true,
                    productoDB
                })
            }
        });
});

//====================================
//          Buscar productos
//====================================

app.get('/productos/buscar/:termino', verificarToken, (req, res) => {

    let termino = req.params.termino;
    let reqex = new RegExp(termino, 'i');

    Producto.find({ nombre: reqex })
        .populate('categoria', 'nombre')
        .exec((err, productos) => {
            if (err) {
                return res.status(500).json({
                    ok: false,
                    err
                });
            }

            res.json({
                OK: true,
                productos
            })
        });


});


//====================================
//      Crear un nuevo producto
//====================================

app.post('/productos', verificarToken, (req, res) => {
    // grabar usuario 
    // grabar categoria 

    let body = req.body;

    let producto = new Producto({
        nombre: body.nombre,
        precioUni: body.precioUni,
        descripcion: body.descripcion,
        disponible: body.disponible,
        categoria: body.categoria,
        usuario: req.usuario._id
    });


    producto.save((err, productoDB) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!productoDB) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.status(201).json({
            ok: true,
            producto: productoDB
        });
    });

});

//====================================
//         Actualizar producto
//====================================

app.put('/productos/:id', verificarToken, (req, res) => {
    // grabar usuario
    // grabar categoria listado
    let id = req.params.id;
    let body = req.body;
    let actProducto = {
        nombre: body.nombre,
        precioUni: body.precioUni,
        descripcion: body.descripcion,
        categoria: body.categoria,
        disponible: body.disponible
    }
    Producto.findByIdAndUpdate(id, actProducto, { new: true, runValidators: true }, (err, productoDB) => {

        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }

        if (!productoDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'El producto no esta en base de datos'
                }
            });
        }

        res.json({
            ok: true,
            producto: productoDB
        });
    });


});

//====================================
//       Borrar producto
//====================================

app.delete('/productos/:id', verificarToken, (req, res) => {
    // grabar el usuario
    // grabar una categoria del listado

    let id = req.params.id;

    let cambiaDisponible = {
        disponible: false
    }

    Producto.findByIdAndUpdate(id, cambiaDisponible, { new: true }, (err, productoBorrado) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }

        if (!productoBorrado) {
            return res.status(400).json({
                ok: false,
                err: {
                    mensage: 'Rroducto no encontrado'
                }
            });
        }

        res.json({
            ok: true,
            producto: productoBorrado,
            message: 'Producto borrado'
        });
    });
});

module.exports = app;